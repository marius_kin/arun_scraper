
# coding: utf-8

# In[1]:


import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import json
# import pandas as pd
# from PIL import Image
import re 
import datetime as dt
from datetime import datetime
from dateutil.parser import parse
import os, sys
 


# In[2]:


date = time.strftime("%Y%m%d")

text_to_populate = 'Enterobase_' + date + '.txt'

# print(text_to_populate)


# In[20]:


def scrape_database():
    

    fp = webdriver.FirefoxProfile()

    fp.set_preference("browser.download.folderList",2)
    fp.set_preference("browser.download.manager.showWhenStarting",False)
    fp.set_preference("browser.download.dir", os.getcwd())
    fp.set_preference("browser.helperApps.neverAsk.saveToDisk", "text/plain")

    browser = webdriver.Firefox(firefox_profile=fp)
#     browser = webdriver.Firefox()

    url = u'http://enterobase.warwick.ac.uk/species/ecoli/search_strains?query=st_search'

    print(url)
    browser.get(url) 
    time.sleep(2)
    
    element = browser.find_element_by_id('st-input')  
    element.send_keys("131")
  
    elements = browser.find_elements_by_class_name('ui-button-text')
    # submit form
    for element in elements:
        if element.text == 'Submit':
            element.click()
            break
    
    
    menu = browser.find_element_by_id("dds-data");
    hidden_submenu = browser.find_element_by_id('dds-save')
    actions = webdriver.common.action_chains.ActionChains(browser)
    actions.move_to_element(menu)
    actions.perform()
    
    
    time.sleep(8)
    
    actions.move_by_offset(xoffset = 0, yoffset=20)
    actions.click()
    actions.perform()
    
    element = browser.find_element_by_id('filename')
    element.send_keys(text_to_populate)
    elements = browser.find_elements_by_tag_name('button')
    
    # Save file
    i = 1
    for element in elements:
        if element.text == 'Save':
            element.click()
            break
    
    time.sleep(10)
    browser.close()
    
    
scrape_database()

